# frozen_string_literal: true

Rails.application.routes.draw do
  resources :policies
  resources :companies
  resources :employees do
    collection do
      get 'import'
      post 'create_multiple'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
