# frozen_string_literal: true

class EmployeesController < ApplicationController
  before_action :set_employee, only: %i[show edit update destroy]

  # GET /employees
  # GET /employees.json
  def index
    @employees = Employee.all
  end

  # GET /employees/1
  # GET /employees/1.json
  def show; end

  # GET /employees/new
  def new
    @employee = Employee.new
  end

  # GET /employees/1/edit
  def edit; end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)

    respond_to do |format|
      if @employee.save
        format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    @companies = Company.all.by_name
  end

  def create_multiple
    file = import_file_params[:file]
    begin
      company = Company.find params[:employee][:company]
    rescue ActiveRecord::RecordNotFound
      flash[:error] = 'Please select a valid company'
      return  redirect_to import_employees_path
    end

    return redirect_to import_employees_path if check_file_error(file)

    # check csv if it is valid
    errors = CsvProcessor::ImportUserCsvValidator.call(file.tempfile)
    if errors
      flash[:error] = errors
      return redirect_to import_employees_path
    end
    company.employee_creator = file
    company.save!
    ImportEmployeeJob.perform_later(company)
    flash[:notice] = 'File has been processed. Please refresh the page after some time'
    redirect_to import_employees_path
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_employee
    @employee = Employee.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def employee_params
    params.require(:employee).permit(:name, :email, :phone, :company_id)
  end

  def import_file_params
    params.require(:employee).permit(:file)
  end

  def check_file_error(file)
    unless file.present? && file.path.present?
      flash[:error] = 'A valid file required. Please upload CSV file'
      return true
    end

    if File.extname(file.path) != '.csv'
      flash[:error] = 'A valid file required. Please upload CSV file'
      return true
    end
    false
  end
end
