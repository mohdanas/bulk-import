# frozen_string_literal: true

require 'csv'

class ImportEmployeeJob < ApplicationJob
  queue_as :default

  def perform(company)
    current_company = company
    file = company.employee_creator.file
    file = file.file if file.is_a?(CarrierWave::SanitizedFile)
    csv = CSV.parse(File.read(file))
    user_list = extract_user_from_csv(csv)
    bad_users = []
    # actually import users
    error_file_url =
      begin
        ActiveRecord::Base.transaction do
          user_list.reduce(bad_users) do |users_array, usr|
            import_user(users_array, usr, current_company)
          end
          raise Error::UserImportError if bad_users.any?
        end
      rescue Error::UserImportError
        create_error_file(bad_users, current_company, csv)
      end
    if error_file_url.present?
      # TODO: - we send error report link via email
    end
  end

  def import_user(bad_users, usr, current_company)
    user = Employee.new(email: usr[:email].to_s.downcase, name: usr[:name], phone: usr[:phone])
    user.company_id = current_company.id
    usr[:assigned_policies].split('|').each do |policy|
      pol = Policy.find_or_create_by(name: policy, company_id: current_company.id)
      user.policies << pol
    end

    if user.save
    # TODO
    else
      if user.errors.details[:email]&.any? { |e| e[:error] == :taken }
        user.errors.delete(:email)
        user.errors.add(
          :email, 'Email already registered'
        )
      end

      user_errors = collect_errors(user)
      bad_users << usr.merge(error: user_errors)
    end

    bad_users
  end

  def collect_errors(user)
    user_errors = user.errors.messages.keys.collect do |key|
      message = user.errors.full_messages_for(key).first
      message.downcase
    end

    user_errors
  end

  # Takes array of bad user objects
  def create_error_file(bad_users, current_company, csv)
    file_name = "tmp/import_error_#{(Time.zone.now.to_f * 1000).to_i}.xlsx"
    error_sheet(bad_users, csv).serialize(file_name)
    current_company.error_report = File.open(file_name, 'r')
    current_company.save
    File.delete(file_name)

    current_company.error_report.url
  end

  # Create the actual csv file from array of bad user objects
  def error_sheet(error_data, csv)
    package = Axlsx::Package.new
    wb = package.workbook
    wb.add_worksheet(name: 'Import Errors') do |sheet|
      header_with_error = clean_and_return_headers(csv)
      bold_text = wb.styles.add_style(b: true)
      sheet.add_row header_with_error, style: bold_text
      error_data.each do |error_hash|
        data_row = []
        row_error_count = error_hash[:error].size
        height = row_error_count.positive? ? 20 * row_error_count : 20
        header_with_error.each do |sym|
          sym = sym.delete('*') if sym['*']
          data_row << if error_hash[sym].is_a?(Array)
                        error_hash[sym].join("\n")
                      else
                        error_hash[sym.to_sym]
                      end
        end
        sheet.add_row data_row, height: height
      end
    end
    package
  end

  def clean_and_return_headers(csv)
    [:error] + csv[0]
  end

  def extract_user_from_csv(csv)
    csv.map.with_index do |row, i|
      next if i.zero?

      {
        name: row[0],
        email: row[1],
        phone: row[2],
        report_to: row[3],
        assigned_policies: row[4]
      }
    end.compact
  end
end
