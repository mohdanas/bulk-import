# frozen_string_literal: true

json.partial! 'policies/policy', policy: @policy
