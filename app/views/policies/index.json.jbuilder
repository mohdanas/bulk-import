# frozen_string_literal: true

json.array! @policies, partial: 'policies/policy', as: :policy
