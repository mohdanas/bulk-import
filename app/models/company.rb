# frozen_string_literal: true

class Company < ApplicationRecord
  mount_uploader :employee_creator, EmployeeUploader
  mount_uploader :error_report, ErrorReportUploader
  serialize :employee_creator, JSON

  has_many :employees

  validates :name, presence: true

  scope :by_name, -> { order(:name) }
end
