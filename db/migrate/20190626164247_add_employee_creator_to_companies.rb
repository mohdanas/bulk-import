class AddEmployeeCreatorToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :employee_creator, :string
    add_column :companies, :error_report, :string
  end
end
