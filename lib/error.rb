# frozen_string_literal: true

module Error
  class UserImportError < StandardError; end
end
