# frozen_string_literal: true

require 'csv'

module CsvProcessor
  class ImportUserCsvValidator
    MAX_SHEET = ENV['CSV_USER_IMPORT_MAX'] || 10_001
    CSV_SHEET_SCHEMA = %w[
      employee_name
      email
      phone
      report_to
      assigned_policies
    ].freeze

    def self.call(file)
      csv = CSV.parse(File.read(file))
      return 'File headers are inaccurate, file cannot be imported ' if csv[0] != CSV_SHEET_SCHEMA
    rescue StandardError => e
      Rails.logger.error "#{e.message}\n#{e.backtrace.join("\n")}"
    end
  end
end
